Feature: Admin Control Panel | Customers | Search

@t8893
Scenario: Search while all fields are empty

  Given I am on the "home" page
  Then the title of the "home" page is "STC Cloud"
  When I log in as "accountManager" user
  Then I get redirected to the "home" page
  When I click on "adminTabButton" on the "home" page
  When I click on "customerTabButton" on the "home" page
  Then "customerTab" is visible on the "home" page
  When I click on "searchButton" on the "home" page
  Then "alertBox" should have "Add one of the search criteria to search for!" text on the "home" page

@t8894
Scenario: Search by not Exist Customer ID

  Given I am on the "home" page
  Then the title of the "home" page is "STC Cloud"
  When I log in as "accountManager" user
  Then I get redirected to the "home" page
  When I click on "adminTabButton" on the "home" page
  When I click on "customerTabButton" on the "home" page
  Then "customerTab" is visible on the "home" page
  When I fill "customerNameInput" with "!@#$%" on the "home" page
  When I click on "searchButton" on the "home" page
  Then "customerResultsParagragh" should have "There are no customers matching your search critria!" text on the "home" page

@t8895
Scenario: Search by not Exist Account ID

  Given I am on the "home" page
  Then the title of the "home" page is "STC Cloud"
  When I log in as "accountManager" user
  Then I get redirected to the "home" page
  When I click on "adminTabButton" on the "home" page
  When I click on "customerTabButton" on the "home" page
  Then "customerTab" is visible on the "home" page
  When I fill "accountIdInput" with "9999999" on the "home" page
  When I click on "searchButton" on the "home" page
  Then "customerResultsParagragh" should have "There are no customers matching your search critria!" text on the "home" page

@t8896
Scenario: Check action menu for a customer that already have linked his billing

  Given I am on the "home" page
  Then the title of the "home" page is "STC Cloud"
  When I log in as "accountManager" user
  Then I get redirected to the "home" page
  When I click on "adminTabButton" on the "home" page
  When I click on "customerTabButton" on the "home" page
  Then "customerTab" is visible on the "home" page
  When I fill "customerNameInput" with "test4341" on the "home" page
  When I click on "searchButton" on the "home" page
  Then "customerResultsTable" is visible on the "home" page
  When I click on "actionButton" on the "home" page
  Then "createDealButton" is visible on the "home" page

@t8902
Scenario: Search by valid customer name having more than 10 marching results

  Given I am on the "home" page
  Then the title of the "home" page is "STC Cloud"
  When I log in as "accountManager" user
  Then I get redirected to the "home" page
  When I click on "adminTabButton" on the "home" page
  When I click on "customerTabButton" on the "home" page
  Then "customerTab" is visible on the "home" page
  When I fill "customerNameInput" with "test" on the "home" page
  When I click on "searchButton" on the "home" page
  Then "customerResultsTable" is visible on the "home" page
  And there are only 10 "customerRow" on the "home" page
