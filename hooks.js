const {client} = require('nightwatch-cucumber');
const {defineSupportCode} = require('cucumber');

defineSupportCode(({After, Before}) => {
  Before(() => client.init());
  After(() => client.end());
});
