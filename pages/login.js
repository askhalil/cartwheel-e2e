module.exports = {
  elements: {
    body: 'body',
    username: '#j_username',
    password: '#j_password',
    loginButton: '#login > section > ul > li:nth-child(5) > input[type="submit"]'
  }
}
