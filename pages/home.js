module.exports = {
  url: 'https://dev-marketplace.stcs.com.sa/',
  elements: {
    body: 'body',
    register: '#main-menu-links > ul > li:nth-child(5) > a',
    adminTabButton: 'body > div.off-canvas-wrap.dashboard > div > div.hide-for-small-only > div > div > div > div > div > div > div.medium-9.hide-for-small-only.no-p.columns.title-bar-top-menu.right-items.text-right > ul > li:nth-child(5) > a',
    loginButton: '#main-menu-links > ul > li:nth-child(4) > a',
    customerTabButton: 'body > div.off-canvas-wrap.dashboard > div > div.row.collapse.full.full-page-hieght > div > div > div.container.main-dashboard-content > section > main > div.row.collapse > ul > li:nth-child(1) > a',
    customerTab: 'body > div.off-canvas-wrap.dashboard > div > div.row.collapse.full.full-page-hieght > div > div > div.container.main-dashboard-content > section > main > div.container > div > div > h1',
    searchButton: 'body > div.off-canvas-wrap.dashboard > div > div.row.collapse.full.full-page-hieght > div > div > div.container.main-dashboard-content > section > main > div.container > div > div > div.row > div > div > form > div:nth-child(4) > button',
    alertBox: '#error-message',
    customerNameInput: '#id_customer_name',
    customerResultsParagragh: 'body > div.off-canvas-wrap.dashboard > div > div.row.collapse.full.full-page-hieght > div > div > div.container.main-dashboard-content > section > main > div.container > div > div > section > p',
    accountIdInput: '#id_account_id',
    customerResultsTable: 'body > div.off-canvas-wrap.dashboard > div > div.row.collapse.full.full-page-hieght > div > div > div.container.main-dashboard-content > section > main > div.container > div > div > section > table',
    actionButton: '#actions-1',
    createDealButton: '#create-deal-1',
    customerRow: 'body > div.off-canvas-wrap.dashboard > div > div.row.collapse.full.full-page-hieght > div > div > div.container.main-dashboard-content > section > main > div.container > div > div > section > table > tbody > tr'
  }
}
