const {client} = require('nightwatch-cucumber');
const {defineSupportCode} = require('cucumber');
const expect = require('expect');
const users = require('../users.js');

defineSupportCode(({Given, Then, When,}) => {
  Given(/^I am on the "(.*?)" page$/, (pageName) => {
    return client
      .page[pageName]()
      .navigate()
      .waitForElementVisible('body', 1000);
  });

  When(/^I log in as "(.*?)" user$/, (user) => {
    const loginButtonMapping = "@loginButton"
    const usernameInputMapping = "@username"
    const passwordInputMapping = "@password"
    const targetUser = users[user]
    return client.perform((client, done) => {
      client
        .page["home"]()
        .waitForElementVisible(loginButtonMapping, 1000)
        .click(loginButtonMapping)
        .waitForElementVisible('body', 1000);
      done();
    }).perform((client, done) => {
      client
        .page["login"]()
        .waitForElementVisible(usernameInputMapping, 1000)
        .setValue(usernameInputMapping, targetUser.username)
        .waitForElementVisible(passwordInputMapping, 1000)
        .setValue(passwordInputMapping, targetUser.password)
        .waitForElementVisible(loginButtonMapping, 1000)
        .click(loginButtonMapping);
      done();
    })
  });

  When(/^I click on "(.*?)" on the "(.*?)" page$/, (buttonName, pageName) => {
    const buttonMapping = `@${buttonName}`;
    return client
      .page[pageName]()
      .waitForElementVisible(buttonMapping, 1000)
      .click(buttonMapping);
  });

  Then(/^the title of the "(.*?)" page is "(.*?)"$/, (pageName, text) => {
    return client
      .page[pageName]()
      .assert
      .title(text);
  });

  Then(/^I get redirected to the "(.*?)" page$/, (pageName) => {
    return client
      .page[pageName]()
      .assert
      .urlContains(client.page[pageName]().url);
  });

  Then(/^"(.*?)" is visible on the "(.*?)" page$/, (elementName, pageName) => {
    const elementMapping = `@${elementName}`;
    return client
      .page[pageName]()
      .waitForElementVisible(elementMapping, 1000);
  });

  Then(/^I wait for (\d+) seconds$/, (seconds) => {
    return client.pause(seconds * 1000);
  });

  Then(/^I fill the following data into the "(.*?)" page$/, (pageName, dataTable) => {
    let page = client.page[pageName]();
    dataTable
      .raw()
      .forEach(r => {
        const inputMapping = `@${r[0]}`;
        page = page
          .waitForElementVisible(inputMapping, 1000)
          .setValue(inputMapping, r[1]);
      });
    return page;
  });

  Then(/^I fill "(.*?)" with "(.*?)" on the "(.*?)" page$/, (elementName, value, pageName) => {
    const elementMapping = `@${elementName}`;
    return client.page[pageName]()
      .waitForElementVisible(elementMapping, 1000)
      .setValue(elementMapping, value);
  });

  Then(/^the following elements with their values should appear in the "(.*?)" page$/, (pageName, dataTable) => {
    let page = client.page[pageName]();
    dataTable
      .raw()
      .forEach(r => {
        const inputMapping = `@${r[0]}`;
        page = page
          .waitForElementVisible(inputMapping, 1000)
          .assert.value(nputMapping, r[1]);
      });
    return page;
  });

  Then(/^"(.*?)" should have "(.*?)" value on the "(.*?)" page$/, (elementName, value, pageName) => {
    const elementMapping = `@${elementName}`;
    return client.page[pageName]()
      .waitForElementVisible(elementMapping, 1000)
      .assert.value(elementMapping, value);
  });

  Then(/^the following elements with their texts should appear in the "(.*?)" page$/, (pageName, dataTable) => {
    let page = client.page[pageName]();
    dataTable
      .raw()
      .forEach(r => {
        const inputMapping = `@${r[0]}`;
        page = page
          .waitForElementVisible(inputMapping, 1000)
          .expect.element(inputMapping).text.to.equal(r[1]);
      });
    return page;
  });

  Then(/^"(.*?)" should have "(.*?)" text on the "(.*?)" page$/, (elementName, value, pageName) => {
    const elementMapping = `@${elementName}`;
    return client.page[pageName]()
      .waitForElementVisible(elementMapping, 1000)
      .expect.element(elementMapping).text.to.equal(value);
  });

  Then(/^there are only (\d+) "(.*?)" on the "(.*?)" page$/, (count, elementName, pageName) => {
    const elementCSS = client.page[pageName]().elements[elementName].selector;
    client.elements('css selector', elementCSS, result => expect(result.value.length).toBe(count))
  });
});
